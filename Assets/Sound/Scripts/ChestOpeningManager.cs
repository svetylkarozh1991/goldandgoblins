using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestOpeningManager : MonoBehaviour
{
    public static ChestOpeningManager instance = null;

    [SerializeField] private AudioSource _musicRewardFlowSource;
    [SerializeField] private AudioSource _swooshSource;
    [SerializeField] private AudioSource _openingChestSource;
    [SerializeField] private AudioSource _finalAwardSource;
    [SerializeField] private AudioSource _viewing�hestSource;
    [SerializeField] private AudioSource _unlocking�ardSource;

    [SerializeField] private AudioClip _musicRewardFlowSound;
    [SerializeField] private AudioClip _swooshSound;
    [SerializeField] private AudioClip _openingChestSound;
    [SerializeField] private AudioClip _finalAwardSound;
    [SerializeField] private AudioClip _viewing�hestdSound;
    [SerializeField] private AudioClip _unlocking�ardSound;

    static private AudioSource s_musicRewardFlowSource;
    static private AudioSource s_swooshSource;
    static private AudioSource s_openingChestSource;
    static private AudioSource s_finalAwardSource;
    static private AudioSource s_viewing�hestSource;
    static private AudioSource s_unlocking�ardSource;

    static private AudioClip s_musicRewardFlowSound;
    static private AudioClip s_swooshSound;
    static private AudioClip s_openingChestSound;
    static private AudioClip s_finalAwardSound;
    static private AudioClip s_viewing�hestdSound;
    static private AudioClip s_unlocking�ardSound;

    private void Start()
    {
        InitializeManager();
        DontDestroyOnLoad(gameObject);
    }


    public static void PlaySoundMusicRewardFlow()
    {
        s_musicRewardFlowSource.PlayOneShot(s_musicRewardFlowSound);
    }

    public static void PlaySound�hestDrop()
    {
        s_swooshSource.PlayOneShot(s_swooshSound);
    }
    public static void PlaySoundOpeningChest()
    {
        s_openingChestSource.PlayOneShot(s_openingChestSound);
    }
    public static void PlaySoundViewing�hest()
    {
        s_viewing�hestSource.PlayOneShot(s_viewing�hestdSound);
    }

    public static void PlaySoundFinalAward()
    {
        s_finalAwardSource.PlayOneShot(s_finalAwardSound);
    }

    public static void PlaySoundUnlocking�ard()
    {
        s_unlocking�ardSource.PlayOneShot(s_unlocking�ardSound);
    }


    private void InitializeManager()
    {
        s_musicRewardFlowSource = _musicRewardFlowSource;
        s_swooshSource = _swooshSource;
        s_openingChestSource = _openingChestSource;
        s_finalAwardSource = _finalAwardSource;
        s_viewing�hestSource = _viewing�hestSource;
        s_unlocking�ardSource = _unlocking�ardSource;

        s_musicRewardFlowSound = _musicRewardFlowSound;
        s_swooshSound = _swooshSound;
        s_openingChestSound = _openingChestSound;
        s_finalAwardSound = _finalAwardSound;
        s_viewing�hestdSound = _viewing�hestdSound;
        s_unlocking�ardSound = _unlocking�ardSound;
    }
}
