using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioManager : MonoBehaviour
{
    public static UIAudioManager instance = null;

    [SerializeField] private  AudioSource _clickSource;
    [SerializeField] private  AudioSource _swooshSource;
    [SerializeField] private  AudioSource _barrelSpawnSource;

    [SerializeField] private  AudioClip _clickSound;
    [SerializeField] private  AudioClip[] _swooshSound;
    [SerializeField] private  AudioClip _barrelSpawnSound;

    static private int s_indexOpenSwoosh;
    static private int s_indexCloseSwoosh;
    static private int s_indexSoundHelper;

    static private AudioSource s_clickSource;
    static private AudioSource s_swooshSource;

    static private AudioClip s_clickSound;
    static private AudioClip[] s_swooshSound;

    private void Start()
    {
        s_indexOpenSwoosh = 0;
        s_indexCloseSwoosh = 1;
        s_indexSoundHelper = 0;

        DontDestroyOnLoad(gameObject);

        InitializeManager();

    }

    public static void PlaySoundIconsOpen()
    {
        s_clickSource.PlayOneShot(s_clickSound);
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexOpenSwoosh]);
    }

    public static void PlaySoundIconsClose()
    {
        s_clickSource.PlayOneShot(s_clickSound);
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexCloseSwoosh]);
    }

    public static void PlaySoundHelper()
    {
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexSoundHelper]);
        s_indexSoundHelper++;
        if (s_indexSoundHelper == s_swooshSound.Length)
            s_indexSoundHelper = 0;
    }
    public static void PlaySoundCardOpen()
    {
        s_swooshSource.PlayOneShot(s_swooshSound[s_indexOpenSwoosh]);
    }


    private void InitializeManager()
    {
        s_clickSource = _clickSource;
        s_swooshSource = _swooshSource;

        s_clickSound = _clickSound;
        s_swooshSound = _swooshSound;
    }
}
