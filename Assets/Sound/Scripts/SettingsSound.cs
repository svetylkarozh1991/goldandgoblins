using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class SettingsSound : MonoBehaviour
{
    [SerializeField] AudioMixerGroup _musicChannel;
    [SerializeField] AudioMixerGroup _effectsChannel;
    [SerializeField] AudioMixerGroup _voiceChannel;

    [SerializeField] Slider _sliderMusic;
    [SerializeField] Slider _sliderEffects;
    [SerializeField] Slider _sliderVoice;

    private int _lowerDecibelLimit;
    private void Awake()
    {
        _lowerDecibelLimit = 20;
        LoadingSettingsSound();
    }

    public void ChangeVolumeMusic(float volume)
    {
        _musicChannel.audioMixer.SetFloat("VolumeMusic", Mathf.Log10(volume)* _lowerDecibelLimit);
    }

    public void ChangeVolumeEffects(float volume)
    {
        _effectsChannel.audioMixer.SetFloat("VolumeEffects", Mathf.Log10(volume)* _lowerDecibelLimit);
    }

    public void ChangeVolumeVoice(float volume)
    {
        _voiceChannel.audioMixer.SetFloat("VolumeVoice", Mathf.Log10(volume)* _lowerDecibelLimit);
    }

    private void LoadingSettingsSound()
    {
        float _value;

        _musicChannel.audioMixer.GetFloat("VolumeMusic", out _value);
        _sliderMusic.value = Mathf.Pow(10,_value / _lowerDecibelLimit);

        _effectsChannel.audioMixer.GetFloat("VolumeEffects", out _value);
        _sliderEffects.value = Mathf.Pow(10, _value / _lowerDecibelLimit);

        _voiceChannel.audioMixer.GetFloat("VolumeVoice", out _value);
        _sliderVoice.value = Mathf.Pow(10, _value / _lowerDecibelLimit);

    }
}
