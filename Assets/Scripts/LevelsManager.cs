﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelsManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _levels;

    public int NomerLevel = 0;
    public Level CurrentLevel;

    public event Action<int> OnChangeLevel;

    public void Init()
    {
        NomerLevel = 0;
        TryMakeNextLevel();
    }

    public void TryMakeNextLevel()
    {
        NomerLevel++;
        if ((NomerLevel < 1) || (NomerLevel > _levels.Length)) NomerLevel = 1;

        DestroyExistLevel();
        MakeLevel();
    }
    private void MakeLevel()
    {
        if (_levels.Length <= NomerLevel - 1)
        {
            Debug.Log("No finded levels for loading");
            return;
        }

        CurrentLevel = GetLoadLevel(_levels[NomerLevel - 1]).GetComponent<Level>();
        EntryPoint.Instance.GoblinsManager.ResetWhenStartNewLevel();
        
    }
    private void DestroyExistLevel()
    {
        if (CurrentLevel!=null)
            CurrentLevel.FinishIt();
    }

    public GameObject GetLoadLevel(GameObject gameObjectPrefab)
    {
        OnChangeLevel?.Invoke(NomerLevel);
        return Instantiate(gameObjectPrefab);
    }

}
