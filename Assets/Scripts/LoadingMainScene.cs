using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingMainScene : MonoBehaviour
{
    [SerializeField] private Image _progressImage;
    [SerializeField] private GameObject _rotationImage;

    private void Start()
    {

        StartCoroutine(AsynchronousLoad("Game"));
    }
    IEnumerator AsynchronousLoad(string scene)
    {
        yield return null;
        AsyncOperation ao = SceneManager.LoadSceneAsync(scene);

        while (!ao.isDone)
        {
            float progress = Mathf.Clamp01(ao.progress / 1f);
            _progressImage.fillAmount = progress;
            //Debug.Log("Loading progress: " + (progress * 100) + "%");
            _rotationImage.transform.Rotate(Vector3.forward, -1.5f);
            yield return null;
        }
    }

    private void Update()
    {
        
    }
}
