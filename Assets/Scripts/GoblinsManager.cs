using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinsManager
{
    private Dictionary<Vector2Int, Goblin> _allGoblins = new Dictionary<Vector2Int, Goblin>();

    private int _limit = 10;
    private int _countGoblinsAndBarrels = 1;
    private int _countBuyGoblins = 0;

    public event Action<int, int> OnChangeCountGoblins;
    public int Limit { get { return _limit; } }
    public int CountGoblins { get { return _countGoblinsAndBarrels; } }

    public void ResetWhenStartNewLevel()
    {
        _countBuyGoblins = 0;
    }
    public bool IsHaveLimitForNewBarrel()
    {
        return _countGoblinsAndBarrels < _limit ? true : false;
    }
    public int CountBuyGoblins
    {
        get { return _countBuyGoblins; }
        set { _countBuyGoblins = value; }
    }
    public void AddedGoblinOrBarrel()
    {
        if (_countGoblinsAndBarrels >= _limit) return;
        _countGoblinsAndBarrels++;
        OnChangeCountGoblins?.Invoke(_countGoblinsAndBarrels, _limit);
    }
    public void GoblinsWasRemoved()
    {
        _countGoblinsAndBarrels--;
        OnChangeCountGoblins?.Invoke(_countGoblinsAndBarrels, _limit);
    }
    public bool IsGoblinExistOnScene(Vector2Int pos)
    {
        if (_allGoblins.ContainsKey(pos))
            return true; 
        else
            return false;
    }
    public void AddGoblinFromScene(Goblin goblin, Vector2Int pos)
    {
        if (!_allGoblins.ContainsKey(pos))
        {
            _allGoblins[pos] = goblin;
        }
    }
    public void RemoveGoblinFromScene(Vector2Int pos)
    {
        _allGoblins.Remove(pos);
    }

    public double PriceForBuyGoblin
    {
        get { return 2 * Math.Pow(4, _countBuyGoblins + 1); }
    }

    public void MakeWorks()
    {
        foreach (Goblin item in _allGoblins.Values)
        {
            item?.MakeWork();
        }
    }
}
