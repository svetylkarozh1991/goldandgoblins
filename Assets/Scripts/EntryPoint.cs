using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    public static EntryPoint Instance;

    public CurrencyManager CurrencyManager;
    public ChestsManager ChestsManager;
    public GoblinsManager GoblinsManager;
    public CardsManager CardsManager;
    public AdsManager AdsManager;
    public SaveLoadManager SaveLoadManager;

    public LevelsManager LevelsManager;
    public PicableResourcesManager PicableResourcesManager;

    public UIManager UIManager;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        CurrencyManager = new CurrencyManager();
        GoblinsManager = new GoblinsManager();
        CardsManager = new CardsManager();
        AdsManager = new AdsManager();
        SaveLoadManager = new SaveLoadManager();

        LevelsManager = Instantiate(LevelsManager);
        PicableResourcesManager = Instantiate(PicableResourcesManager);

        LevelsManager.Init();
        UIManager.Init();

        //emitter counts after all inits
        CurrencyManager.Coins.ChangeCount(20);
        CurrencyManager.Bottles.ChangeCount(50);
        CurrencyManager.Gems.ChangeCount(30);
    }
    void Update()
    {
        GoblinsManager.MakeWorks();
    }
}
