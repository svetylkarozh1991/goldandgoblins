using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PickableObject : MonoBehaviour
{

    [SerializeField] private Transform _3Dmodel;
    [SerializeField] public AnimatorFlightResources _animatorFlightGold;
    [SerializeField] private TMPro.TMP_Text _countText;
    [SerializeField] public double Count;

    private Vector2Int _thisPosition;
    private void Start()
    {
        _thisPosition = new Vector2Int((int)(transform.position.x), (int)(transform.position.z));
        EntryPoint.Instance.LevelsManager.CurrentLevel.AddPickableObjectFromScene(this, _thisPosition);
    }

    public virtual void PickUpThisObject() { }

    public void OnPickUp()
    {

        _3Dmodel.DOKill();
        EntryPoint.Instance.LevelsManager.CurrentLevel.DestroyPickableFromScene(_thisPosition);

        Destroy(gameObject);
    }
    public void SetCount(double count)
    {
        Count = count;
        _countText.text = Count.ToString();
    }

}
