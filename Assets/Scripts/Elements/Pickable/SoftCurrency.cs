using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SoftCurrency : PickableObject
{
    public override void PickUpThisObject()
    {
        Vector2 StackPosition = RectTransformUtility.WorldToScreenPoint(CameraInstance.Instance.MyCamera, transform.position);

        _animatorFlightGold.SpawnElixirs(StackPosition, (int)Count);
        EntryPoint.Instance.CurrencyManager.Bottles.ChangeCount(Count);
        OnPickUp();
    }
    private void OnMouseDown()
    {
        PickUpThisObject();
    }

}
