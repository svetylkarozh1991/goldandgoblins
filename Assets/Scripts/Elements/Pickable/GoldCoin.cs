using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GoldCoin : PickableObject
{
    public override void PickUpThisObject()
    {
        Vector2 StackPosition = RectTransformUtility.WorldToScreenPoint(CameraInstance.Instance.MyCamera, transform.position);

        _animatorFlightGold.SpawnStackGold(StackPosition);

        EntryPoint.Instance. CurrencyManager.Coins.ChangeCount(Count);
        OnPickUp();
    }

    private void OnMouseDown()
    {
        PickUpThisObject();
    }

}
