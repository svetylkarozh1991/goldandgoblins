using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HardCurrency : PickableObject
{
    public override void PickUpThisObject()
    {
        Vector2 StackPosition = RectTransformUtility.WorldToScreenPoint(CameraInstance.Instance.MyCamera, transform.position);

        _animatorFlightGold.SpawnGems(StackPosition, (int)Count);
        EntryPoint.Instance.CurrencyManager.Gems.ChangeCount(Count);
        OnPickUp();
    }
    private void OnMouseDown()
    {
        PickUpThisObject();
    }
}
