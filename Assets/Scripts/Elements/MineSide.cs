﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TMPro;

public class MineSide : MonoBehaviour
{

    [SerializeField] private GameObject _inactiveMine;
    [SerializeField] private ButtonForUpgrade _buttonForUpgrade;
    [SerializeField] private GameObject _buttonForAuto;
    [SerializeField] private GameObject _checkForAuto;

    [SerializeField] private GameObject _coinGO;

    [SerializeField] private GameObject _upperCircle;
    private CircleAndText _topCircleAndText;

    [SerializeField] private GameObject _topTextGo;
    [SerializeField] private TextMeshProUGUI _topText;



    [SerializeField] private DestroyableObject _destroyableObject;

    [Header("Gem part")]
    [SerializeField] private SpriteRenderer _gemSprite;
    [SerializeField] private List<Sprite> _listImageTypeCard;

    [Header("Nomer Mine")]
    [SerializeField] private int index = 1;
    public int Level = 1;
    private bool IsAutomationIncome = false;

    private bool _cartAnimationBegining = false;
    private float _speed = 7.5f;

    private TimerForCircle _timerForMining;

    private void Start()
    {
        _destroyableObject.ActionWhenDurabilityIsOver += IsClearedFromRocks;


        _gemSprite.sprite = _listImageTypeCard[index];
        _topCircleAndText = _upperCircle.GetComponent<CircleAndText>();


        _buttonForUpgrade.gameObject.SetActive(false);

        _buttonForUpgrade.SetParams(Balance.SideMineStartPrice(index), Balance.SideMineMultyPricePerLVL(index), Level, Balance.MineSideBorderValues[index-1]);
        _buttonForUpgrade.SetCalback(OnSetLevel);

        OnSetLevel(Level);
        _speed = Balance.SideMineSpeed(index);

        _buttonForAuto.SetActive(true);

        _timerForMining = gameObject.AddComponent<TimerForCircle>();
        _timerForMining.InitTimerForCircle(_speed, new Dictionary<double, Action> { { 0, OnTimerMakeMoney }, { 1, OnTimerStartCartAnimation } }, _topCircleAndText);
        _timerForMining.Pause();
    }

    private void OnDestroy()
    {
        _destroyableObject.ActionWhenDurabilityIsOver -= IsClearedFromRocks;
    }
    private void OnTimerMakeMoney()
    {
        if (IsAutomationIncome)
        {
            _cartAnimationBegining = false;
            EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(Balance.SideMineIncomePerTime(index, Level));
        }
        else
        {
            _timerForMining.Pause();
            ShowCoinForManualGetting();
        }
    }

    private void OnTimerStartCartAnimation()
    {
        if (_cartAnimationBegining) return;

        _cartAnimationBegining = true;
        //TODO старт анимации тележки
    }
    public void SetAutomatization ()
    {
        _buttonForAuto.SetActive(false);
        _checkForAuto.SetActive(true);
        IsAutomationIncome = true;
    }

    private void ShowCoinForManualGetting()
    {
        _coinGO.SetActive(true);
    }
    public void ClickedCoin()
    {
        _cartAnimationBegining = false;
        _coinGO.SetActive(false);
        EntryPoint.Instance.CurrencyManager.Coins.ChangeCount(Balance.SideMineIncomePerTime(index, Level));
        _timerForMining.Unpause();
    }

    private void OnSetLevel(int newLevel)
    {
        Level = newLevel;
        _topText.text = SpecialFunctions.GetStringFromBigDouble(Balance.SideMineIncomePerTime(index, Level));
    }

    private void IsClearedFromRocks()
    {
        EffectsAudioManager.PlaySoundClearingRubble();
        _inactiveMine.SetActive(false);
        _upperCircle.SetActive(true);
        _topTextGo.SetActive(true);
        _buttonForUpgrade.gameObject.SetActive(true);
        _timerForMining.Unpause();
    }

}
