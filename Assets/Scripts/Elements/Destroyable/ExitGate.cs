using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGate : MonoBehaviour
{
    [Header("Button")]
    [SerializeField] private Button _buttonNextLevel;
    [SerializeField] private GameObject _gameObjectButtonNextLevel;


    [SerializeField] private DestroyableObject _destroyableObject;

    private void Start()
    {
        _destroyableObject.ActionWhenDurabilityIsOver +=IsClearedFromRocks;
        _buttonNextLevel.onClick.AddListener(ClickOnBtn);
    }

    private void OnDestroy()
    {
        _destroyableObject.ActionWhenDurabilityIsOver -= IsClearedFromRocks;
        _buttonNextLevel.onClick.RemoveAllListeners();
    }

    public void ClickOnBtn()
    {
        EntryPoint.Instance.LevelsManager.TryMakeNextLevel();
    }

    private void IsClearedFromRocks()
    {
        _gameObjectButtonNextLevel.SetActive(true);
    }

}
