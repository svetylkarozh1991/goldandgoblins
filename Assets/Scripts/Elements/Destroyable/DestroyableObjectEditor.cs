using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class DestroyableObjectEditor : MonoBehaviour
{
    [Header("Durability Level")]
    [SerializeField] private int _level;
    private DestroyableObject _mainScript;


    private void Start()
    {
        _mainScript = GetComponent<DestroyableObject>();
    }

    private void Update()
    {
        _mainScript.Level = _level;
    }
}
