﻿using UnityEngine;
using System.Collections;

[ExecuteAlways]
public class EditorFixPositionElements : MonoBehaviour
{
#if UNITY_EDITOR
    private void Update()
    {
        transform.position = new Vector3(Mathf.Round(transform.position.x), 0, Mathf.Round(transform.position.z));
    }
#endif 
}
