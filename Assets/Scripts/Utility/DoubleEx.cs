﻿using System;

public static class DoubleEx
{
    public static string ToReductionText(this double value)
    {
        int discharge = 0;
        double dischargeMultiplier = 0.001d;

        while (Math.Truncate(value) > 999)
        {
            discharge++;
            value *= dischargeMultiplier;
        }

        ReductionTextSimple reductionText = new ReductionTextSimple();
        var reductionString = reductionText.At(discharge);

        var fractionalPart = Math.Truncate((value - Math.Truncate(value)) * 10);
        var textFractionalPart = fractionalPart > 0 ? $".{fractionalPart}" : "";

        string result = $"{Math.Truncate(value)}{textFractionalPart}" + reductionString;

        return result;
    }
}
