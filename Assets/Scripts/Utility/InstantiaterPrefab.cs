﻿using UnityEngine;

public class InstantiaterPrefab<T> where T : Object
{
    private T _prefab;

    public InstantiaterPrefab() : this($"Prefabs/{typeof(T).Name}") { }
    public InstantiaterPrefab(string pathPrefab)
    {
        _prefab = Resources.Load<T>(pathPrefab);

        if (_prefab == null)
            throw new System.Exception($"Prefab not found in \"Assets/Resorces/{pathPrefab}\" path.");
    }

    public T Create()
    {
        return GameObject.Instantiate(_prefab);
    }
    public T Create(Vector3 globalPosition)
    {
        return GameObject.Instantiate(_prefab, globalPosition, Quaternion.identity);
    }
    public T Create(Transform parent)
    {
        return GameObject.Instantiate(_prefab, parent);
    }
}
