﻿using UnityEngine;
using UnityEngine.UI;

public class TestLimit : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private Button _button2;
    [SerializeField] private LimitedValue _limitedValue;

    private void Awake()
    {
        _limitedValue.Init(0, 10);

        _button.onClick.AddListener(() => _limitedValue.TryIncrease());
        _button2.onClick.AddListener(() => _limitedValue.IncreaseLimit());
    }

    private void OnDestroy()
    {
        _button.onClick.RemoveAllListeners();
        _button2.onClick.RemoveAllListeners();
    }

}
