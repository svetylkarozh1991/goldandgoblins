﻿using System.Collections.Generic;

public class ReductionTextSimple
{
    private List<string> _listReductionText = new List<string>() 
    {"", "K", "M", "B", "T", 
        "aa", "ab", "ac", "ad", "ae", "af", "ag",
        "ba", "bb", "bc", "bd", "be", "bf", "bg"};

    public string At(int discharge)
    {
        if (discharge > _listReductionText.Count)
            return $"e+{discharge}";

        return _listReductionText[discharge];
    }
}