﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AllCards
{
    public List<OneCard> AllCardsData = new List<OneCard>();

    public AllCards()
    {
        InitCards();

        AllCardsData[0].CountHaveCards = 2;
        AllCardsData[1].CountHaveCards = 1;
        AllCardsData[0].Level = 1;
        AllCardsData[1].Level = 1;
    }

    private void InitCards()
    {
        AllCardsData = new List<OneCard>();
        AllCardsData.Add(new OneCard(0, CaptionCard.income, TypeCard.forge, 0, RareCard.Common));
        AllCardsData.Add(new OneCard(1, CaptionCard.income, TypeCard.amethist, 0, RareCard.Common));
        AllCardsData.Add(new OneCard(2, CaptionCard.income, TypeCard.amethist, 0, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(3, CaptionCard.income, TypeCard.citraine, 0, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(4, CaptionCard.income, TypeCard.rocks, 0, RareCard.Rare));
        AllCardsData.Add(new OneCard(5, CaptionCard.timeToApear, TypeCard.goblins, 0, RareCard.Rare));
        AllCardsData.Add(new OneCard(6, CaptionCard.income, TypeCard.mineshafts, 0, RareCard.Unique));
        AllCardsData.Add(new OneCard(7, CaptionCard.limit, TypeCard.goblins, 0, RareCard.Unique));

        //Изначально закрытые
        AllCardsData.Add(new OneCard(8, CaptionCard.income, TypeCard.citraine, 3, RareCard.Common));
        AllCardsData.Add(new OneCard(9, CaptionCard.income, TypeCard.agate, 4, RareCard.Common));
        AllCardsData.Add(new OneCard(10, CaptionCard.income, TypeCard.topaz, 5, RareCard.Common));
        AllCardsData.Add(new OneCard(11, CaptionCard.income, TypeCard.agate, 6, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(12, CaptionCard.income, TypeCard.opal, 7, RareCard.Common));
        AllCardsData.Add(new OneCard(13, CaptionCard.speed, TypeCard.mineshafts, 8, RareCard.Common));
        AllCardsData.Add(new OneCard(14, CaptionCard.income, TypeCard.topaz, 9, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(15, CaptionCard.income, TypeCard.jade, 10, RareCard.Common));
        AllCardsData.Add(new OneCard(16, CaptionCard.income, TypeCard.opal, 12, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(17, CaptionCard.income, TypeCard.onix, 14, RareCard.Common));
        AllCardsData.Add(new OneCard(18, CaptionCard.discount, TypeCard.checkpoints, 15, RareCard.Rare));
        AllCardsData.Add(new OneCard(19, CaptionCard.income, TypeCard.jade, 17, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(20, CaptionCard.income, TypeCard.saphire, 19, RareCard.Common));
        AllCardsData.Add(new OneCard(21, CaptionCard.income, TypeCard.checkpoints, 20, RareCard.Unique));
        AllCardsData.Add(new OneCard(22, CaptionCard.income, TypeCard.onix, 22, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(23, CaptionCard.critChance, TypeCard.onix, 23, RareCard.Rare));
        AllCardsData.Add(new OneCard(24, CaptionCard.income, TypeCard.tormaline, 24, RareCard.Common));
        AllCardsData.Add(new OneCard(25, CaptionCard.income, TypeCard.saphire, 27, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(26, CaptionCard.income, TypeCard.deliveries, 30, RareCard.Rare));
        AllCardsData.Add(new OneCard(27, CaptionCard.income, TypeCard.aquamarine, 31, RareCard.Common));
        AllCardsData.Add(new OneCard(28, CaptionCard.income, TypeCard.tormaline, 33, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(29, CaptionCard.critDamage, TypeCard.goblins, 36, RareCard.Unique));
        AllCardsData.Add(new OneCard(30, CaptionCard.income, TypeCard.emerald, 38, RareCard.Common));
        AllCardsData.Add(new OneCard(31, CaptionCard.timeToApear, TypeCard.checkpoints, 40, RareCard.Rare));
        AllCardsData.Add(new OneCard(32, CaptionCard.income, TypeCard.aquamarine, 42, RareCard.Uncommon));
        AllCardsData.Add(new OneCard(33, CaptionCard.income, TypeCard.diamond, 47, RareCard.Common));
        AllCardsData.Add(new OneCard(34, CaptionCard.levelPlus, TypeCard.goblins, 50, RareCard.Unique));

        //Ограничился 50-ым уровнем. Хотя в игре даже близко его не будет
    }

    public string GetRareText(RareCard rare)
    {
        return rare switch
        {
            RareCard.Common => "Обычная",
            RareCard.Uncommon => "Необычная",
            RareCard.Rare => "Редкая",
            RareCard.Unique => "Легендарная",
            _ => "Обычная",
        };

    }
}
