﻿using UnityEngine;
using System.Collections;

public enum TypeCard
{
    forge,
    rocks,
    mineshafts,
    goblins,
    amethist,
    citraine,
    agate,
    topaz,
    opal,
    jade, //нефрит
    onix,
    checkpoints,
    saphire,
    tormaline,
    deliveries,
    aquamarine,
    emerald,
    diamond

    //diamond открывается на 47 уровне. и хватит пожалуй.

}
public enum CaptionCard
{
    income = 0,
    speed = 1,
    limit = 2,
    timeToApear =3,
    discount = 4,
    critChance = 5,
    critDamage = 6,
    levelPlus = 7
}

public enum RareCard
{
Common,
Uncommon,
Rare,
Unique
}


public class OneCard
{
    public int Index = 0;
    /////////////////

    public CaptionCard Caption;
    public TypeCard Type;

    public int RoundForOpen;

    public int Level = 0;
    public int CountHaveCards = 0;

    public RareCard Rare;

    public OneCard(int index, CaptionCard caption, TypeCard type, int roundForOpen, RareCard rare =  RareCard.Common)
    {
        Index = index;
        Rare = rare;

        Caption = caption;
        Type = type;



        RoundForOpen = roundForOpen;
    }

    public string GetDiscription()
    {
        return Type switch
        {
            TypeCard.forge => "Кузница приносит больше дохода",
            TypeCard.rocks => "Из породы можно добыть больше монет",
            TypeCard.mineshafts => "Улучшает работу боковых шахт",
            TypeCard.goblins => "Улучшает работу гоблинов",
            TypeCard.amethist => "Шахта Аметист приносит больше дохода и автоматизируется",
            TypeCard.citraine => "Шахта Цитраин приносит больше дохода и автоматизируется",
            TypeCard.agate => "Шахта Агат приносит больше дохода и автоматизируется",
            TypeCard.topaz => "Шахта Топаз приносит больше дохода и автоматизируется",
            TypeCard.opal => "Шахта Опал приносит больше дохода и автоматизируется",
            TypeCard.jade => "Шахта Нефрит приносит больше дохода и автоматизируется",
            TypeCard.onix => "Шахта Оникс приносит больше дохода и автоматизируется",
            TypeCard.checkpoints => "Дает крутые бонусы",
            TypeCard.saphire => "Шахта Сапфир приносит больше дохода и автоматизируется",
            TypeCard.tormaline => "Шахта Турмлин приносит больше дохода и автоматизируется",
            TypeCard.deliveries => "Ускоряет доставку",
            TypeCard.aquamarine => "Шахта Аквамарин приносит больше дохода и автоматизируется",
            TypeCard.emerald => "Шахта Изумруд приносит больше дохода и автоматизируется",
            TypeCard.diamond => "Шахта Алмаз приносит больше дохода и автоматизируется",
            _ => throw new System.NotImplementedException(),
        };

    }

    public string GetRareText()
    {
        return EntryPoint.Instance.CardsManager.Cards.GetRareText(Rare);
    }
    public string GetFileNameImage()
    {
        int tempNomer = Index % 11 + 1;
        return "CardsImages\\ca" + tempNomer.ToString(); 
    }

    public string GetTextRoundNeedForOpen()
    {
        return "Рудник " + RoundForOpen.ToString();
    } 
    public string GetTextLevel()
    {
        return "Уровень " + Level.ToString();
    }

    public int GetIndexTypeImage()
    {
        //in OneCardUI have List<Sprite> _listImageTypeCard
        return Type switch
        {
            TypeCard.forge => 2,
            TypeCard.rocks => 1,
            TypeCard.goblins => 0,
            TypeCard.amethist => 2,
            TypeCard.citraine => 2,
            TypeCard.agate => 2,
            TypeCard.topaz => 2,
            TypeCard.opal => 2,
            TypeCard.jade => 2,
            TypeCard.onix => 2,
            TypeCard.saphire => 2,
            TypeCard.tormaline => 2,
            TypeCard.aquamarine => 2,
            TypeCard.emerald => 2,
            TypeCard.diamond => 2,

            //maybe need new icons
            TypeCard.mineshafts => 2,
            TypeCard.checkpoints => 1,
            TypeCard.deliveries => 0,
            _ => 0,
        };
    }
    public string GetTypeText()
    {
        switch (Type)
        {
            case TypeCard.forge:
                return "Кузница";
            case TypeCard.rocks:
                return "Порода";
            case TypeCard.mineshafts:
                return "Шахты";
            case TypeCard.goblins:
                return "Гоблины";
            case TypeCard.amethist:
                return "Аметист";
            case TypeCard.citraine:
                return "Цитрин";
            case TypeCard.agate:
                return "Агат";
            case TypeCard.topaz:
                return "Топаз";
            case TypeCard.opal:
                return "Опал";
            case TypeCard.jade:
                return "Нефрит";
            case TypeCard.onix:
                return "Оникс";
            case TypeCard.checkpoints:
                return "Рубежи";
            case TypeCard.saphire:
                return "Сапфир";
            case TypeCard.tormaline:
                return "Турмалин";
            case TypeCard.deliveries:
                return "Доставки";
            case TypeCard.aquamarine:
                return "Аквамарин";
            case TypeCard.emerald:
                return "Изумруд";
            case TypeCard.diamond:
                return "Алмаз";
            default:
                Debug.LogError("Ошибка");
                return "Кузница";
        }
    }


    public string GetCaptionText()
    {
        switch (Caption)
        {
            case CaptionCard.income:
                return "Доход";
            case CaptionCard.speed:
                return "Скорость";
            case CaptionCard.limit:
                return "Лимит+";
            case CaptionCard.timeToApear:
                return "Время до появления";
            case CaptionCard.discount:
                return "Скидка";
            case CaptionCard.critChance:
                return "Шанс критического удара";
            case CaptionCard.critDamage:
                return "Критический урон";
            case CaptionCard.levelPlus:
                return "Уровень+";

            default:
                Debug.LogError("Ошибка");
                return "Доход";
        }
    }

    public int CountNeedCards()
    {
        switch (Level)
        {
            case 0: return 2;
            case 1: return 5;
            case 2: return 10;
            case 3: return 25;
            case 4: return 50;
            case 5: return 100;
            default:
                return CountNeedCardsRecursion(Level - 6) * 100;
        }
    }
    private int CountNeedCardsRecursion(int lvl=-1)
    {
        if (lvl == -1)
            lvl = Level;

        switch (lvl)
        {
            case 0: return 2;
            case 1: return 5;
            case 2: return 10;
            case 3: return 25;
            case 4: return 50;
            case 5: return 100;
            default:
                return CountNeedCardsRecursion(lvl - 6) * 100;
        }
    }
    public void TryLevelUp()
    {

    }


    //ЕСЛИ ТУТ ОШИБКА ТО ВЕСЬ ЭТОТ МЕТОД TestSwitch МОЖНО УДАЛЯТЬ.
    public string TestSwitch()
    {
        return Caption switch
        {
            CaptionCard.income => "Доход",
            CaptionCard.speed => "Скорость",
            CaptionCard.limit => "Лимит+",
            _ => "Доход",
        };
    }




    /* Цены прокачки в бутылках.
     * обычная
     * 2 - 50
     * 5 -100
     * 10 -200 
     * 
     * 100 карточек - 800
     * 200 -1500
     * 
     * необычная
     * 5 -200
     * 10 -400
     * 50 - 2000
     * 
     * редкая
     * 2 -500
     * 5 -2000
     * 
     * 
     */
}
