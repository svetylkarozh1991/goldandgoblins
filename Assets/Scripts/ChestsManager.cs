using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ChestType
{
    Wood,
    Iron,
    Gold
}
public enum ChestTextures
{
    ChestWoodClosed,
    ChestIronClosed,
    ChestGoldClosed,
    ChestWoodOpen,
    ChestIronOpen,
    ChestGoldOpen
}

public class ChestsManager : MonoBehaviour
{
    [Header("Textures")]
    [SerializeField] private Texture _chestWoodClosed;
    [SerializeField] private Texture _chestIronClosed;
    [SerializeField] private Texture _chestGoldClosed;
    [SerializeField] private Texture _chestWoodOpen;
    [SerializeField] private Texture _chestIronOpen;
    [SerializeField] private Texture _chestGoldOpen;

    [Header("Chests")]
    [SerializeField] private ChestControler _chestWood;
    [SerializeField] private ChestControler _chestIron;
    [SerializeField] private ChestControler _chestGold;


    public readonly long NeedMillisecondForNextChest = 240 * 60 * 1000; //4h

    public long TimeForFullFreeChests { set; get; }
    public Texture GetClosedChestTexture(ChestType type)
    {
        switch (type)
        {
            case ChestType.Wood:
                return _chestWoodClosed;
            case ChestType.Iron:
                return _chestIronClosed;
            case ChestType.Gold:
                return _chestGoldClosed;
            default: 
                return null;
        }
    }

    public ChestControler GetChestControler(ChestType type)
    {
        return type switch
        {
            ChestType.Wood => _chestWood,
            ChestType.Iron => _chestIron,
            ChestType.Gold => _chestGold,
            _ => throw new System.NotImplementedException()
        };
    }

    public Texture GetTexture(ChestTextures texture)
    {
        switch (texture)
        {
            case ChestTextures.ChestWoodClosed:
                return _chestWoodClosed;
            case ChestTextures.ChestIronClosed:
                return _chestIronClosed;
            case ChestTextures.ChestGoldClosed:
                return _chestGoldClosed;
            case ChestTextures.ChestWoodOpen:
                return _chestWoodOpen;
            case ChestTextures.ChestIronOpen:
                return _chestIronOpen;
            case ChestTextures.ChestGoldOpen:
                return _chestGoldOpen;
            default:
                return null;
        }
    }

    public void AddFreeTimeForFullFreeChests()
    {
        TimeForFullFreeChests += NeedMillisecondForNextChest;
    }

    public string GetCaption(ChestType type)
    {
        switch (type)
        {
            case ChestType.Wood:
                return "���������� ������";
            case ChestType.Iron:
                return "�������� ������";
            case ChestType.Gold:
                return "������� ������";
            default:
                return null;
        }
    }
}