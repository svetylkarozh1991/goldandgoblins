﻿using UnityEngine;

[RequireComponent(typeof(Movement))]
public class MoveningSwap : MonoBehaviour
{
    private Movement _movement;

    private Transform _thisTransform;
    private Vector3 _initialPosition;
    private Vector3 _presumptivePosition;
    private bool _isMoveToPresumptivePosition;

    private Vector3[] _directions;

    private void Awake()
    {
        _thisTransform = GetComponent<Transform>();
        _movement = GetComponent<Movement>();

        _initialPosition = _thisTransform.position;

        _directions = new Vector3[8]
        {
            new Vector3( 0f, 0f, -1f),
            new Vector3(-1f, 0f, -1f),
            new Vector3( 1f, 0f, -1f),
            new Vector3(-1f, 0f,  0f),
            new Vector3( 1f, 0f,  0f),
            new Vector3( 0f, 0f,  1f),
            new Vector3(-1f, 0f,  1f),
            new Vector3( 1f, 0f,  1f),
        };
    }
    
    public bool CanMove()
    {
        return _initialPosition == _presumptivePosition;
    }
    public void MoveToPresumptivePosition(Vector3 positionIfNotFindPresumptive)
    {
        _presumptivePosition = FindSuitablePositionToMove(positionIfNotFindPresumptive);

        _isMoveToPresumptivePosition = true;
        _movement.MoveTo(_presumptivePosition);
    }
    public void MoveToInitialPosition()
    {
        _isMoveToPresumptivePosition = false;
        _movement.MoveTo(_initialPosition);
    }
    public void FixInitialPosition()
    {
        if (_isMoveToPresumptivePosition == true)
        _initialPosition = _presumptivePosition;
    }

    public void FixInitialPosition(Vector3 position)
    {
        _initialPosition = position;
    }

    public bool IsInitialPosition(Vector3 position)
    {
        return position == _initialPosition;
    }

    private Vector3 FindSuitablePositionToMove(Vector3 position)
    {
        foreach(var direction in _directions)
        {
            var suitablePosition = _initialPosition + direction;

            var hasPlane = IsHasComponentFromDirection<Plane>(suitablePosition, Vector3.down);
            var hasObstacleDrag = IsHasComponentFromDirection<ObstacleDrag>(suitablePosition, Vector3.down);
            //var hasGoblin = IsHasComponentFromDirection<Goblin>(_initialPosition + new Vector3(0f, 0.5f, 0f), direction);
            var hasGoblin = IsHasComponentFromDirection<Goblin>(suitablePosition, Vector3.down);

            var isFreePosition = EntryPoint.Instance.LevelsManager.CurrentLevel.IsPlaceFreeByPos((int)suitablePosition.x, (int)suitablePosition.z); // kekw

            if (hasPlane == true && hasObstacleDrag == false && hasGoblin == false && isFreePosition == true)                
            {
                return suitablePosition;
            }
        }

        return position;
    }
    private bool IsHasComponentFromDirection<T>(Vector3 position, Vector3 direction, float distance = 1f)
    {
        var raycastHits = Physics.RaycastAll(position, direction, distance);

        foreach(var item in raycastHits)
        {
            if (item.collider.TryGetComponent<T>(out _) == true)
            {
                return true;
            }
        }

        return false;
    }
}
