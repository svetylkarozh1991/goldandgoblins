using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DragAndDropXZ : MonoBehaviour
{
    public static bool IsWorkNow;

    public UnityEvent OnDragStart;
    public UnityEvent OnDrag;
    public UnityEvent OnDragEnd;

    private Transform _thisTransform;
    private float _startY;

    private Vector3 _screenPosition;
    private Vector3 _offset;

    [SerializeField] private float _offsetYOnDrag = 0.2f;

    private void Awake()
    {        
        _thisTransform = GetComponent<Transform>();
        _startY = _thisTransform.position.y;
    }

    private void OnMouseDown()
    {
        GoblinAudioManager.PlayVoiceSoudSpawnDragging();
        OnDragStart?.Invoke();
        IsWorkNow = true;

        _thisTransform.position = new Vector3(
            _thisTransform.position.x, _thisTransform.position.y + _offsetYOnDrag, _thisTransform.position.z);


        _screenPosition = Camera.main.WorldToScreenPoint(_thisTransform.position);

        _offset = _thisTransform.position - Camera.main.ScreenToWorldPoint(
            new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPosition.z));
    }

    private void OnMouseDrag()
    {
        OnDrag?.Invoke();

        _screenPosition = Camera.main.WorldToScreenPoint(_thisTransform.position);

        Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPosition.z);     
        Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + _offset;


        _thisTransform.position = new Vector3(currentPosition.x, _startY + _offsetYOnDrag, currentPosition.z);
    }

    private void OnMouseUp()
    {
        OnDragEnd?.Invoke();
        IsWorkNow = false;

        _thisTransform.position = new Vector3(_thisTransform.position.x, _startY, _thisTransform.position.z);
    }

}
