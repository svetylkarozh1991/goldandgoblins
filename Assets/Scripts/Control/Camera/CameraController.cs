using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    [Header("")]
    [SerializeField] private float _activeCLickTime;
    [SerializeField] private float _touchTime = 0.15f;
    [SerializeField] private float _accelerationTime=0.5f;

    [SerializeField] private float _upScreenCurrent;
    [SerializeField] private float _downScreenCurrent = 9f;

    private bool _dragPanMoveActive;
    private float _startDownMousePosition;
    private float _deltaMousePosition;
    private float _startPositionZ;

    private void Update()
    {
        //if (EntryPoint.Instance.UIManager.IsNeedStopCamera) return; need code for alow camera moving

        if (Input.GetMouseButtonDown(0))
        {
            _startPositionZ = transform.position.z;
            _dragPanMoveActive = true;
            _startDownMousePosition = Input.mousePosition.y;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _dragPanMoveActive = false;
            _activeCLickTime = 0f;
        }


        if (_dragPanMoveActive && DragAndDropXZ.IsWorkNow == false)
        {
            _activeCLickTime += Time.deltaTime;
            _deltaMousePosition = _startDownMousePosition - Input.mousePosition.y;
            float resultPos = _startPositionZ + _deltaMousePosition*0.01f;


            if (resultPos > _upScreenCurrent)
                resultPos = _upScreenCurrent;
            
            if (resultPos < _downScreenCurrent)
                resultPos = _downScreenCurrent;
   
            if (_activeCLickTime > _touchTime)
            {
                transform.DOLocalMoveZ(resultPos, _accelerationTime);
            }

        }
    }
}

