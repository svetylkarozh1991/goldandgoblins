using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObjectsRotation : MonoBehaviour
{
    void Start()
    {
        transform.DORotate(new Vector3(0f, 360f, 0f), 5f, RotateMode.LocalAxisAdd)
            .SetLoops(-1, LoopType.Restart)
            .SetEase(Ease.Linear);
        transform.DOMoveY(1f, 1.5f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.Linear);
            
    }

    private void OnDestroy()
    {
        transform.DOKill();
    }

}
