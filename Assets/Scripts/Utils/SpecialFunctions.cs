﻿using UnityEngine;
using System.Collections;
using System;

public class SpecialFunctions
{
    public static string GetStringFromBigDouble(Double num)
    {
        if (num >= 1000000000000000000000000000000000000000000d)
        {
            return (num).ToString("0.0e+0");
        }
        else if (num >= 100000000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000000001).ToString("0") + "bb";
        else if (num >= 10000000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000000001).ToString("0.0") + "bb";
        else if (num >= 1000000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000000001).ToString("0.00") + "bb";
        else if (num >= 100000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000001).ToString("0") + "ba";
        else if (num >= 10000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000001).ToString("0.0") + "ba";
        else if (num >= 1000000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000000001).ToString("0.00") + "ba";
        else if (num >= 100000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000001).ToString("0") + "ag";
        else if (num >= 10000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000001).ToString("0.0") + "ag";
        else if (num >= 1000000000000000000000000000000000d) return (num * 0.000000000000000000000000000000001).ToString("0.00") + "ag";
        else if (num >= 100000000000000000000000000000000d) return (num * 0.000000000000000000000000000001).ToString("0") + "af";
        else if (num >= 10000000000000000000000000000000d) return (num * 0.000000000000000000000000000001).ToString("0.0") + "af";
        else if (num >= 1000000000000000000000000000000d) return (num * 0.000000000000000000000000000001).ToString("0.00") + "af";
        else if (num >= 100000000000000000000000000000d) return (num * 0.000000000000000000000000001).ToString("0") + "ae";
        else if (num >= 10000000000000000000000000000d) return (num * 0.000000000000000000000000001).ToString("0.0") + "ae";
        else if (num >= 1000000000000000000000000000d) return (num * 0.000000000000000000000000001).ToString("0.00") + "ae";
        else if (num >= 100000000000000000000000000d) return (num * 0.000000000000000000000001).ToString("0") + "ad";
        else if (num >= 10000000000000000000000000d) return (num * 0.000000000000000000000001).ToString("0.0") + "ad";
        else if (num >= 1000000000000000000000000d) return (num * 0.000000000000000000000001).ToString("0.00") + "ad";
        else if (num >= 100000000000000000000000d) return (num * 0.000000000000000000001).ToString("0") + "ac";
        else if (num >= 10000000000000000000000d) return (num * 0.000000000000000000001).ToString("0.0") + "ac";
        else if (num >= 1000000000000000000000d) return (num * 0.000000000000000000001).ToString("0.00") + "ac";
        else if (num >= 100000000000000000000d) return (num * 0.000000000000000001).ToString("0") + "ab";
        else if (num >= 10000000000000000000) return (num * 0.000000000000000001).ToString("0.0") + "ab";
        else if (num >= 1000000000000000000) return (num * 0.000000000000000001).ToString("0.00") + "ab";
        else if (num >= 100000000000000000) return (num * 0.000000000000001).ToString("0") + "aa";
        else if (num >= 10000000000000000) return (num * 0.000000000000001).ToString("0.0") + "aa";
        else if (num >= 1000000000000000) return (num * 0.000000000000001).ToString("0.00") + "aa";
        else if (num >= 100000000000000) return (num * 0.000000000001).ToString("0") + "T";
        else if (num >= 10000000000000) return (num * 0.000000000001).ToString("0.0") + "T";
        else if (num >= 1000000000000) return (num * 0.000000000001).ToString("0.00") + "T";
        else if (num >= 100000000000) return (num * 0.000000001).ToString("0") + "B";
        else if (num >= 10000000000) return (num * 0.000000001).ToString("0.0") + "B";
        else if (num >= 1000000000) return (num * 0.000000001).ToString("0.00") + "B";
        else if (num >= 100000000) return (num * 0.000001).ToString("0") + "M";
        else if (num >= 10000000) return (num * 0.000001).ToString("0.0") + "M";
        else if (num >= 1000000) return (num * 0.000001).ToString("0.00") + "M";
        else if (num >= 100000) return (num * 0.001).ToString("0") + "K";
        else if (num >= 10000) return (num * 0.001).ToString("0.0") + "K";
        else if (num >= 1000) return (num * 0.001).ToString("0.00") + "K";
        else
        if ((num % 1 >= 0.01) && (num % 1 <= 0.09))
        {
            return num.ToString("0.00");
        }
        else
        if (num % 1 >= 0.1)
        {
            return num.ToString("0.0");
        }
        else
        {
            return num.ToString("0");
        }
    }
}
