using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFlightResources : MonoBehaviour
{
    [SerializeField] private GameObject _stackGold;
    [SerializeField] private GameObject _stackElixirs;
    [SerializeField] private GameObject _stackGems;
    [SerializeField] private CanvasResources _canvasResources;

    private int _quantityElixirs;
    private int _quantityGems;


    public void SpawnStackGold(Vector2 spawnPosition)
    {
        CheckSpawnCanvas();
        Instantiate(_stackGold, spawnPosition, Quaternion.identity, _canvasResources.GetCanvasResources());
        EffectsAudioManager.PlaySoundGold();
    }

    public void SpawnElixirs(Vector3 spawnPosition, int quantityElixirs)
    {
        CheckSpawnCanvas();
        _quantityElixirs = quantityElixirs;
        Instantiate(_stackElixirs, spawnPosition, Quaternion.identity, _canvasResources.GetCanvasResources());
        EffectsAudioManager.PlaySoundElixir();
        
    }

    public void SpawnGems(Vector3 spawnPosition, int quantityGems)
    {
        CheckSpawnCanvas();
        _quantityGems = quantityGems;
        Instantiate(_stackGems, spawnPosition, Quaternion.identity, _canvasResources.GetCanvasResources());
        EffectsAudioManager.PlaySoundGem();
    }

    public int GetQuantityElixirs()
    {
        return _quantityElixirs;
    }

    public int GetQuantityGems()
    {
        return _quantityGems;
    }

    private void CheckSpawnCanvas()
    {
        if (_canvasResources.GetIsCanvasAdded())
        {
            _canvasResources.SpawnCanvas();
        }
    }
}
