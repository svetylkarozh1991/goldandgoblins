using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FlightResource : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Vector2 _target;
    [SerializeField] private RectTransform _rectTransformResource;
    [SerializeField] private CanvasResources _canvasResources;

    private float _delayTime;
    private Transform _targetHeight;

    public void InitAndSetDelayTime(float delayTime, Vector2 deltaPosition)
    {
        _delayTime = delayTime;

        _targetHeight = _canvasResources.GetTargetHeight();
        _target = new Vector2(_target.x, _targetHeight.localPosition.y);

        _rectTransformResource.DOAnchorPos(_target - deltaPosition, _speed).SetSpeedBased().SetEase(Ease.Linear).SetDelay(1 + _delayTime).
            OnComplete(() => { gameObject.SetActive(false); });
    }

    public void SetDelayTime(float delayTime)
    {
        _delayTime = delayTime;
    }
}
