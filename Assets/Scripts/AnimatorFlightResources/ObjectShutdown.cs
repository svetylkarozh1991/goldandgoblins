using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectShutdown : MonoBehaviour
{
    [SerializeField] private GameObject _animatorFlightResources;

    private float _shutdownTime = 0;
    private float _lifetimeObject = 8f;

    private void Update()
    {
        _shutdownTime += Time.deltaTime;

        if (_shutdownTime >= _lifetimeObject)
        {
            _shutdownTime = 0;
            Destroy(_animatorFlightResources.gameObject);
        }
    }
}
