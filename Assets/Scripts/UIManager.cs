using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{


    [Header("Top panels")]
    [SerializeField] private UIUpperPanelLevel _uiUpperPanelLevel;
    [SerializeField] private UIUpperPanelCurrency _uiUpperPanelCurrency;

    [Header("Bottom buttons")]
    [SerializeField] private Button _cardsButton;
    [SerializeField] private Button _chestButton;


    [Header("Panels")]
    [SerializeField] private GameObject _cardsPanel;
    [SerializeField] private GameObject _marketPanel;
    [SerializeField] private GameObject _notFindedCardPanel;
    [SerializeField] private GameObject _findedCardPanel;
    [SerializeField] private GameObject _gainADS;
    [SerializeField] private GameObject _coinDelivery;
    [SerializeField] private GameObject _settingsPanel;

    [Header("Chests")]
    [SerializeField] private UIBuyChest _buyChest;
    private GameObject _buyChestUI;

    [SerializeField] private OpenChestScreenUI _openChestScreenUI;

    [Header("Images")]
    [SerializeField] private Image _imageChestButton;
    [SerializeField] private List<Sprite> _listImageColoredBtns;

    [Header("Scripts")]
    [SerializeField] private PanelNotFindedCard _panelNotFindedCard;
    [SerializeField] private PanelFindedCard _panelFindedCard;



    private bool _needStopCamera;

    public void Init()
    {
        _cardsButton.onClick.AddListener(ShowCardsPanel);
        _chestButton.onClick.AddListener(ShowMarketPanel);

        _buyChestUI = _buyChest.gameObject;

        _uiUpperPanelCurrency.Init();
        _uiUpperPanelLevel.Init();
    }

    private void OnDestroy()
    {
        _cardsButton.onClick.RemoveAllListeners();
        _chestButton.onClick.RemoveAllListeners();
    }


    public void ResetSceens()
    {
        _cardsPanel.SetActive(false);
        _marketPanel.SetActive(false);
        _gainADS.SetActive(false);
        _settingsPanel.SetActive(false);
        _buyChestUI.SetActive(false);
        _needStopCamera = false;
    }

    public void ShowGainADS()
    {
        ResetSceens();
        _gainADS.SetActive(true);
        _needStopCamera = true;
    }

    public void ShowOpenChestScreenUI()
    {
        _openChestScreenUI.gameObject.SetActive(true);
        _needStopCamera = true;
    }

    public UIBuyChest ShowAndGetChestUI()
    {
        _buyChestUI.SetActive(true);
        return _buyChest;
    }

    public void ShowCardsPanel()
    {
        ResetSceens();
        _cardsPanel.SetActive(true);
        _needStopCamera=true;
    }
    public void ShowMarketPanel()
    {
        ResetSceens();
        _marketPanel.SetActive(true);
        _needStopCamera=true;
    }

    public void ShowSettingsPanel()
    {
        ResetSceens();
        _settingsPanel.SetActive(true);
        _needStopCamera = true;
    }

    public bool IsNeedStopCamera { get { return _needStopCamera; }}

    public void ClickedOnCard(int nomer)
    {
        if (EntryPoint.Instance.CardsManager.Cards.AllCardsData[nomer].CountHaveCards == 0 && EntryPoint.Instance.CardsManager.Cards.AllCardsData[nomer].Level==0)
        {
            _notFindedCardPanel.SetActive(true);
            _panelNotFindedCard.GenerateScreen(nomer);
        }
        else if(EntryPoint.Instance.CardsManager.Cards.AllCardsData[nomer].Level > 0)
        {
            _findedCardPanel.SetActive(true);
            _panelFindedCard.GenerateScreen(nomer);
        }
    }

}
