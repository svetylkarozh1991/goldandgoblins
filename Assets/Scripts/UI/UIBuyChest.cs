using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class UIBuyChest : MonoBehaviour
{

    [SerializeField] private GameObject _windowGameObject;
    [SerializeField] private UIWindowBackground _windowBackground;
    [SerializeField] private ChestUI _chestUI;

    [SerializeField] private Button _freeBtn;
    [SerializeField] private TextMeshProUGUI _freeBottomText;
    [SerializeField] private TextMeshProUGUI _freeBottomTextTime;


    private void Awake()
    {
        _freeBtn.onClick.AddListener(ClickFreeChest);
        _windowBackground.GetBtnClose.onClick.AddListener(() => { gameObject.SetActive(false); });

        _windowBackground.SwichToOneLine();
    }
    private void OnDestroy()
    {
        _freeBtn.onClick.RemoveAllListeners();
        _windowBackground.GetBtnClose.onClick.RemoveAllListeners();
    }

    public void AnimateWindow()
    {
        _windowGameObject.transform.DOKill();
        _windowGameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        _windowGameObject.transform.DOScale(1, 0.5f);
    }

    public void GenerateWood()
    {
        AnimateWindow();
        _windowBackground.SetText("���������");

        _chestUI.MakeScreen("��������� ������", ChestTextures.ChestWoodClosed, 50, 61, 10, RareCard.Uncommon);
    }

    public void ClickFreeChest()
    {

    }

}
