using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MarketUI : MonoBehaviour
{
    [SerializeField] private Button _btnChest1;
    [SerializeField] private TextMeshProUGUI _Chest1text2;
    [SerializeField] private TextMeshProUGUI _Chest1textNext;
    [SerializeField] private TextMeshProUGUI _Chest1textTime;



    private void Awake()
    {
        _btnChest1.onClick.AddListener(ClickBtnChest1);
    }
    private void OnDestroy()
    {
        _btnChest1.onClick.RemoveAllListeners();
    }

    public void ClickBtnChest1()
    {
        EntryPoint.Instance.UIManager.ShowAndGetChestUI().GenerateWood();
        EntryPoint.Instance.ChestsManager.AddFreeTimeForFullFreeChests();
        OnEnable();

    }

    private void OnEnable()
    {
        if (EntryPoint.Instance == null) return;

        long deltaTimeForFullFreeChest = EntryPoint.Instance.ChestsManager.TimeForFullFreeChests - DateTime.UtcNow.Millisecond;
        int countFreeChests = 2;
        if (deltaTimeForFullFreeChest > 0) countFreeChests = 1;
        if (deltaTimeForFullFreeChest > EntryPoint.Instance.ChestsManager.NeedMillisecondForNextChest) countFreeChests = 0;

        if (countFreeChests == 2)
        {
            _Chest1text2.gameObject.SetActive(true);
            _Chest1textNext.gameObject.SetActive(false);
            _Chest1textTime.gameObject.SetActive(false);
        }
        else
        {
            _Chest1text2.gameObject.SetActive(false);
            _Chest1textNext.gameObject.SetActive(true);
            _Chest1textTime.gameObject.SetActive(true);
            _Chest1textTime.text = TimeConv.Format((deltaTimeForFullFreeChest % EntryPoint.Instance.ChestsManager.NeedMillisecondForNextChest) * 0.001);
        }
    }
}
