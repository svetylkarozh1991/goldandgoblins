using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class OpenChestScreenUI : MonoBehaviour
{
    private enum State { Init, WaitClick, ShowCards, Finish }

    [SerializeField] private RawImage _chestImage;
    [SerializeField] private RectTransform _rtChestImage;
    [SerializeField] private TextMeshProUGUI _chestCaption;
    [SerializeField] private TextMeshProUGUI _chestBottom;

    [SerializeField] private GameObject _cardsCountObject;
    [SerializeField] private TextMeshProUGUI _cardsCountText;

    private ChestControler _currentChestController;
    private State _state;

    private void Awake()
    {
        _rtChestImage = _chestImage.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        Init(ChestType.Wood);
    }
    private void OnDisable()
    {
        SetState( State.Finish);
        _currentChestController.CloseChest();
    }

    private void SetState(State state)
    {
        switch (state)
        {
            case State.Init:
                break;
            case State.WaitClick:

                break;
            case State.ShowCards:
                _cardsCountObject.SetActive(false);
                break;
            case State.Finish:
                break;
        }

        _state = state;
    }

    public void Init(ChestType type)
    {
        _currentChestController = EntryPoint.Instance.ChestsManager.GetChestControler(type);
        SetState(State.Init);
        _rtChestImage.DOKill();

        _cardsCountObject.SetActive(false);
        _chestCaption.gameObject.SetActive(true);
        _chestCaption.text = EntryPoint.Instance.ChestsManager.GetCaption(type);
        _chestBottom.gameObject.SetActive(false);
        _rtChestImage.localScale = Vector3.one;
        _rtChestImage.anchoredPosition = new Vector3(0, 3000, 0);
        _chestImage.texture = EntryPoint.Instance.ChestsManager.GetClosedChestTexture(type);

        AnimationStage1();
    }

    private void AnimationStage1()
    {
        _rtChestImage.DOAnchorPosY(800, 0.5f).OnComplete(() => { _chestBottom.gameObject.SetActive(true); SetState(State.WaitClick); });
    }
    private void ShowCards()
    {
        SetState(State.ShowCards);
        _rtChestImage.DOScale(0.5f, 0.5f);
        _rtChestImage.DOAnchorPosY(0, 0.5f);
        _currentChestController.OpenChest(0.5f);
        _chestBottom.gameObject.SetActive(false);
        _chestCaption.gameObject.SetActive(false);

        _cardsCountObject.SetActive(true);

    }

    private void Update()
    {
        if ((_state==State.WaitClick) && (Input.GetMouseButtonUp(0)))
        {
            ShowCards();
        }
    }

}
