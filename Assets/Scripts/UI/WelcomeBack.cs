using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WelcomeBack : MonoBehaviour
    {
        [Header("Game object")]
        [SerializeField] private GameObject _panelWB;

        [Header("Button x2")]
        [SerializeField] private Image _imageButton;
        [SerializeField] private Sprite _spriteButtonDefault;
        [SerializeField] private Sprite _spriteButtonADS;

        [Header("Picture")]
        [SerializeField] private Image _imagePicture;
        [SerializeField] private Sprite _spriteUpdate;
        [SerializeField] private Sprite _spriteTV;
        
        // [Header("Rotate picture")]
        // [SerializeField] private Transform _transformImage; // rename in "_rotateImage"

        [Header("Setting")]
        [SerializeField] private float _delayADS; // 3s

        private void Awake()
        {
            _panelWB.SetActive(false);
        }

        private void Start()
        {
            ButtonDefault();
            _panelWB.SetActive(true);
            
            StartCoroutine(Delay());
        }

        private void Update()
        {
            // _transformImage.transform.DORotate(new Vector3(0, 0, -180), 3f);

            // Vector3 rot = new Vector3(0, 360, 0);
            //
            // _transformImage.transform.DORotate(rot, 2f)
            //     .SetLoops(-1, LoopType.Incremental);

        }

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(_delayADS);
            ButtonAds();
        }

        public void OnClick()
        {
            _panelWB.SetActive(false);
        }

        private void ButtonDefault()
        {
            _imageButton.sprite = _spriteButtonDefault;
            _imagePicture.sprite = _spriteUpdate;
        }
        
        private void ButtonAds()
        {
            _imageButton.sprite = _spriteButtonADS;
            _imagePicture.sprite = _spriteTV;
            _imagePicture.color = new Color(1f, 1f, 1f, 1f);

            Vector3 vector3 = new Vector3(1, 1, 1);
        }
    }
}
