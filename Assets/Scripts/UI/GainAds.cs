using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GainAds : MonoBehaviour
{
    [Header("Game object")]
    [SerializeField] private GameObject _gainAds;

    public void OnClickButton()
    {
        _gainAds.SetActive(false);
    }

    public void OnClickGain()
    {
        _gainAds.SetActive(true);
    }
}
