using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIBottomPanel : MonoBehaviour
{
    [Header("Center btn")]
    [SerializeField] private Image _imageCenterBtn;
    [SerializeField] private List<Sprite> _listImageCenterBtn;
    [SerializeField] private TMP_Text _TextPrice;
    [SerializeField] private TMP_Text _TextLevelNewGoblins;
    [SerializeField] private Image _imageFillProgresLevel;

    [SerializeField] GameObject _barrel;
    private Vector3[] _freePos = new Vector3[14];
    private Vector3 _createBarrelPos;
    private double _money;

    private int _barrelCountLine = 6;

    public PathType pathType = PathType.CatmullRom;
    public Vector3[] _waypoints = new Vector3[5];
    private void Start()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount += OnChangeMoney;
        UpdateCenterBtn();
        _freePos[0] = new Vector3(2, 0, 5);
    }

    private void OnDestroy()
    {
        EntryPoint.Instance.CurrencyManager.Coins.OnChangeCount -= OnChangeMoney;
    }
    private void OnChangeMoney(double newMoneyValue)
    {
        _money = newMoneyValue;
        UpdateCenterBtn();
    }

    private void UpdateCenterBtn()
    {
        int countBuyGoblins = EntryPoint.Instance.GoblinsManager.CountBuyGoblins;

        _TextLevelNewGoblins.text = "���� " + (1 + (int)(countBuyGoblins / 8)).ToString();
        _imageFillProgresLevel.fillAmount = (countBuyGoblins % 8) / 8f;

        _TextPrice.text = SpecialFunctions.GetStringFromBigDouble(EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin);

        if (EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin <= _money)
        {
            _imageCenterBtn.sprite = _listImageCenterBtn[0];
        }
        else
            _imageCenterBtn.sprite = _listImageCenterBtn[1];

    }
    public void ClickOnCenterButton()
    {
        
        if (_imageCenterBtn.sprite == _listImageCenterBtn[0])
            EffectsAudioManager.PlaySoundMortarShot();
        
        var isFreePosition = false;
        for (int i = 0; i < _freePos.Length; i++)
        {
            if (i <= _barrelCountLine)
                _freePos[i] = new Vector3(2 + i, 0, 5);
            if (i > _barrelCountLine)
            {
                _freePos[i] = new Vector3(15 - i, 0, 6);
            }
            isFreePosition = EntryPoint.Instance.LevelsManager.CurrentLevel.IsPlaceFreeByPos((int)_freePos[i].x, (int)_freePos[i].z);
            if (isFreePosition)
            {
                _createBarrelPos = _freePos[i];



                if (EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin > _money) return;
                if (!EntryPoint.Instance.GoblinsManager.IsHaveLimitForNewBarrel()) return;

                if (EntryPoint.Instance.GoblinsManager.PriceForBuyGoblin <= _money)
                {
                    EntryPoint.Instance.GoblinsManager.CountBuyGoblins++;
                }

                UpdateCenterBtn();

                Cannon canon = FindObjectOfType<Cannon>();
                canon.CanonRotation(_createBarrelPos);

                _waypoints[0] = canon.transform.position;
                _waypoints[1] = new Vector3(_createBarrelPos.x + ((canon.transform.position.x - _createBarrelPos.x) * 0.4f), canon.transform.position.y + 1, _createBarrelPos.z - ((_createBarrelPos.z - canon.transform.position.z) * 0.4f));
                _waypoints[2] = new Vector3(_createBarrelPos.x + ((canon.transform.position.x - _createBarrelPos.x) * 0.5f), canon.transform.position.y + 2, _createBarrelPos.z - ((_createBarrelPos.z - canon.transform.position.z) * 0.5f));
                _waypoints[3] = new Vector3(_createBarrelPos.x + ((canon.transform.position.x - _createBarrelPos.x) * 0.6f), canon.transform.position.y + 1, _createBarrelPos.z - ((_createBarrelPos.z - canon.transform.position.z) * 0.6f));
                _waypoints[4] = _createBarrelPos;

                Instantiate(_barrel, canon.transform.position, Quaternion.identity);
                BarrelScript barrel = FindObjectOfType<BarrelScript>();
                Destroy(barrel);
                Tween move = barrel.transform.DOPath(_waypoints, 0.2f, pathType).SetLookAt(0.001f);
                move.SetEase(Ease.Linear);
                Destroy(barrel.gameObject, 0.2f);
                StartCoroutine(Wait());
                OnDestroy();
                break;
            }

        }


        IEnumerator Wait()
        {
            yield return new WaitForSeconds(0.19f);
            int levelForNextGoblin = 1 + (int)(EntryPoint.Instance.GoblinsManager.CountBuyGoblins / 8);
            Instantiate(_barrel, _createBarrelPos, Quaternion.identity, EntryPoint.Instance.LevelsManager.CurrentLevel.gameObject.transform).GetComponent<BarrelScript>().InitBarrel(levelForNextGoblin);
        }
    }
}
