using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestUI : MonoBehaviour
{
    [SerializeField] private UIRareLine _rareLine;
    [SerializeField] private ChestImageByRendererTexture _chest;
    [SerializeField] private TextMeshProUGUI _bottleText;
    [SerializeField] private TextMeshProUGUI _cardsText;

    [Header("bottom panel")]
    [SerializeField] private GameObject _bottomBlockTotall;
    [SerializeField] private GameObject _bottomBlock1;
    [SerializeField] private GameObject _bottomBlock2;
    [SerializeField] private GameObject _bottomBlock3;
    [SerializeField] private TextMeshProUGUI _bottomBlockText1Count;
    [SerializeField] private TextMeshProUGUI _bottomBlockText2Count;
    [SerializeField] private TextMeshProUGUI _bottomBlockText3Count;


    /// <param name="rare"> param maximum quality for cards </param>
    public void MakeScreen(string topText, ChestTextures chest, int bottlemin, int bottlemax, int cardsCount, RareCard rare)
    {
        _rareLine.GenerateLineNoCards(topText);
        _chest.SetChestTexure(chest);

        _bottleText.text  = $"{bottlemin}-{bottlemax}";
        _cardsText.text = $"{cardsCount}";

        switch (rare)
        {
            case RareCard.Common:
                _bottomBlockTotall.SetActive(false);
                break;
            case RareCard.Uncommon:
                _bottomBlockTotall.SetActive(true);
                _bottomBlock1.SetActive(true);
                _bottomBlock2.SetActive(false);
                _bottomBlock3.SetActive(false);
                _bottomBlockText1Count.text = $"x{Math.Floor(1 + cardsCount * 0.09d)}";
                break;
            case RareCard.Rare:
                _bottomBlockTotall.SetActive(true);
                _bottomBlock1.SetActive(true);
                _bottomBlock2.SetActive(true);
                _bottomBlock3.SetActive(false);
                _bottomBlockText1Count.text = $"x{Math.Floor(1 + cardsCount * 0.249d)}";
                _bottomBlockText2Count.text = $"x{Math.Floor(1 + cardsCount * 0.1d)}";
                break;
            case RareCard.Unique:
                _bottomBlockTotall.SetActive(true);
                _bottomBlock1.SetActive(true);
                _bottomBlock2.SetActive(true);
                _bottomBlock3.SetActive(true);
                _bottomBlockText1Count.text = $"x{Math.Floor(1 + cardsCount * 0.19d)}";
                _bottomBlockText2Count.text = $"x{Math.Floor(1 + cardsCount * 0.1d)}";
                _bottomBlockText3Count.text = $"x{Math.Floor(1 + cardsCount * 0.0025d)}";
                break;
            default:
                break;
        }

        _bottomBlockText1Count.text = "x2";
    }

}
