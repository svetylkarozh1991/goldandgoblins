using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PanelFindedCard : MonoBehaviour
{
    [SerializeField] private OneCardUI _card;
    [SerializeField] private UIRareLine _bottomRareLine;


    [SerializeField] private TMP_Text _caption;


    public void GenerateScreen(int indexCard)
    {
        _card.InitCardByIndex(indexCard);



        _bottomRareLine.GenerateLine(_card);

    }
}
