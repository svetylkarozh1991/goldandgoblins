﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class CardsPanel : MonoBehaviour
{
    [SerializeField] private GameObject _cardPrefab;

    [Header("Panels")]
    [SerializeField] private TMP_Text _countFindedCards;
    [SerializeField] private RectTransform _rtClosePanel;
    [SerializeField] private RectTransform _rtNotFindedPanel;
    [SerializeField] private RectTransform _rtFindedPanel;

    [Header("ContainerView")]
    [SerializeField] private RectTransform _rtContainerView;
    [SerializeField] private RectTransform _rtViewPort;

    [Header("TopOrangeLines")]
    [SerializeField] private RectTransform _rtTopOrangeLine1;
    [SerializeField] private RectTransform _rtTopOrangeLine2;
    [SerializeField] private RectTransform _rtTopOrangeLine3;

    private List<OneCardUI> _allCardsScripts;
    private List<GameObject> _allCards;

    private float _totalHeight;
    private float _posOrange2;
    private float _posOrange3;

    private float _viewPortHeight;

    private void Start()
    {
        _allCards = new List<GameObject>();
        _allCardsScripts = new List<OneCardUI>();
        for (int i = 0; i < EntryPoint.Instance.CardsManager.Cards.AllCardsData.Count-1; i++)
        {
            _allCards.Add(Instantiate(_cardPrefab));
            _allCardsScripts.Add(_allCards[i].GetComponent<OneCardUI>());
            _allCardsScripts[i].InitCardByIndex(i);
        }

        Regen();
    }

    public void Regen()
    {
        //Count card each panel for calculate correct positions
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;

        _viewPortHeight = _rtViewPort.rect.height;

        for (int i = 0; i < _allCards.Count-1; i++)
        {
            if (_allCardsScripts[i].Card.RoundForOpen > EntryPoint.Instance.LevelsManager.NomerLevel)
            {
                _allCards[i].transform.SetParent(_rtClosePanel, false);
                _allCards[i].transform.localPosition = new Vector2(340*((count3) % 3)-340, 0- 150 - 430*((count3) / 3));
                count3++;
            }
            else if (_allCardsScripts[i].Card.CountHaveCards == 0 && _allCardsScripts[i].Card.Level==0)
            {
                _allCards[i].transform.SetParent(_rtNotFindedPanel, false);
                _allCards[i].transform.localPosition = new Vector2(340 * ((count2) % 3) - 340, 0 - 150 - 430 * ((count2) / 3));
                count2++;
            }
            else
            {
                _allCards[i].transform.SetParent(_rtFindedPanel, false);
                _allCards[i].transform.localPosition = new Vector2(340 * ((count1) % 3) - 340, 0 - 150 - 430 * ((count1) / 3));
                count1++;
            }
        }

        _countFindedCards.text = count1.ToString() + "/" + (_allCards.Count - 1).ToString();

        _rtFindedPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 135 + 430 * ((2 + count1) / 3));
        _rtClosePanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 135 + 430 * ((2 + count3) / 3));
        _rtNotFindedPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 135 + 430 * ((2 + count2) / 3));

        _rtNotFindedPanel.anchoredPosition = new Vector2(0, -135 - 430 * ((2+count1) / 3));
        _rtClosePanel.anchoredPosition = new Vector2(0, -135 - 430 * ((2 + count1) / 3) - 135 - 430 * ((2 + count2) / 3));

        _posOrange2 = -135 - 430 * ((2 + count1) / 3);
        _posOrange3 = _posOrange2 - 135 - 430 * ((2 + count2) / 3);

        _rtTopOrangeLine1.anchoredPosition = new Vector2(0, 0);
        _rtTopOrangeLine2.anchoredPosition = new Vector2(0, _posOrange2);
        _rtTopOrangeLine3.anchoredPosition = new Vector2(0, _posOrange3);

        _totalHeight = 405 + 430 * ((2 + count1) / 3 + (2 + count2) / 3 + (2 + count3) / 3);
        _rtContainerView.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _totalHeight);
    }

    public void OnMoveScrollViewer(Vector2 value)
    {
        float posY = ((float)(value.y) -1) * (_totalHeight - _viewPortHeight);

        if (_posOrange3 > posY)
        {
            _rtTopOrangeLine3.anchoredPosition = new Vector2(0, posY);
        }
        else if (_posOrange2 > posY)
        {
            _rtTopOrangeLine3.anchoredPosition = new Vector2(0, _posOrange3);
            _rtTopOrangeLine2.anchoredPosition = new Vector2(0, posY);
        }
        else if (0 > posY)
        {
            _rtTopOrangeLine1.anchoredPosition = new Vector2(0, posY);
            _rtTopOrangeLine2.anchoredPosition = new Vector2(0, _posOrange2);
            _rtTopOrangeLine3.anchoredPosition = new Vector2(0, _posOrange3);
        }
        else
        {
            _rtTopOrangeLine1.anchoredPosition = new Vector2(0, 0);
            _rtTopOrangeLine2.anchoredPosition = new Vector2(0, _posOrange2);
            _rtTopOrangeLine3.anchoredPosition = new Vector2(0, _posOrange3);
        }
    }
}
